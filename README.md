# 互联网卷B同学玩转物联网开发入门课



> 1. 安全说明：开发板是精密电子产品，怕水怕静电怕短路，特别是通电后，千万不要短路，不要直接触摸金属部分、针脚等！
> 2. 最新版本：[https://gitee.com/honestqiao/iot_beginner](https://gitee.com/honestqiao/iot_beginner)



### 学习硬件：

* 开发板：ESP32-C3 开发板

  <img src="设备资料/PIN_DEF.png" alt="PIN_DEF" style="zoom:50%;" />

* 元器件：

  * SHT30温湿度传感器：蓝色I2C板，或者绿色串口版
  * 红绿灯模块
  * 光敏二极管模块
  * 按键模块



### 入门步骤：

* 刷机：
  
  * 刷机前准备
    * ESP32串口名称确认：
      * [Windows系统](http://hli.aliyuncs.com/haas-static/haasapi/index.html#/Python/docs/zh-CN/startup/ESP32_startup?id=windows系统)
      * [MAC系统](http://hli.aliyuncs.com/haas-static/haasapi/index.html#/Python/docs/zh-CN/startup/ESP32_startup?id=mac系统)
      * [Linux]([linux下ESP32烧录步骤（纯命令行） - 程序员大本营 (pianshen.com)](https://www.pianshen.com/article/5347916157/))
    * 固件：
      * 固件使用 ES32-C3固件/esp32c3-20220102-v1.17.zip 解压后的.bin文件
      * 固件文件：**esp32c3-20220102-v1.17.bin**
    * 烧录工具：
      * Windows：esptool，或者VsCode + Haas Studio
        * 下载 刷机工具/esptool-v3.2-win64.zip ，解压后，进入目录。 
      * macOS：esptool，或者VsCode + Haas Studio
        * 下载 刷机工具/esptool-v3.2-macos.zip ，解压后，进入目录。 
      * Linux：esptool
        * esptool-v3.2-linux-amd64.zip：https://github.com/espressif/esptool/releases/tag/v3.2，命令参考Windows环境
    * 开发工具：
      * Windows: MuEditor
        * 下载 编码工具/ `Mu-Editor-Win64-1.1.0b7.zip` 并解压后安装
      * macOS：MuEditor
        * 下载 编码工具/ `Mu-Editor-macOS.dmg` 并解压后安装
      * Linux：Thonny
        * Thonny：[Thonny, Python IDE for beginners](https://thonny.org/)
    
  * 使用VsCode + Haas Studio刷机：【Win10及以下、macOS Big Sur】
  
    * VsCode安装Haas Studio插件
  
      * 插件安装参考：[HaaS Studio安装](http://hli.aliyuncs.com/haas-static/haasapi/index.html#/Python/docs/zh-CN/startup/ESP32_startup?id=haas-studio安装)
  
    * 打开VsCode中的Haas Studio的烧录工具，选择正确的串口，以及上述准备好的固件
  
      * 烧录操作参考：[固件烧录过程](http://hli.aliyuncs.com/haas-static/haasapi/index.html#/Python/docs/zh-CN/startup/ESP32_startup?id=固件烧录过程)
  
    * 烧录后，在下部的输出窗口，按上键，调出烧录执行的指令，把0x1000修改为0x00，然后回车，再次刷机
  
      ```
      esptool.py -b 460800 -p 串口号 write_flash 0x00 固件文件
      ```
  
  * 使用esptool刷机：【Win11及以下、macOS Monterey及以下、Linux】【**推荐使用**】
  
    * 打开cmd，复制命令，然后将`COM3`、`d:\esp32c3-20220102-v1.17.bin` 替换成实际串口号、固件文件，回车开始刷机
    
    * ```
      #Windows
      set PATH=%PATH%;%CD%
      
      #macOS、Linux
      export PATH="$PATH:$(pwd)"
      
      # 请根据系统，设置对应的COM端口和固件地址
      esptool -b 460800 -p COM3 erase_flash
      esptool -b 460800 -p COM3 write_flash 0x00 d:\esp32c3-20220102-v1.17.bin
      
      # 以下为输出结果
      esptool.py v3.2
      Serial port COM3
      Connecting....
      Detecting chip type... ESP32-C3
      Chip is ESP32-C3 (revision 3)
      Features: Wi-Fi
      Crystal is 40MHz
      MAC: 7c:df:a1:d6:55:14
      Uploading stub...
      Running stub...
      Stub running...
      Changing baud rate to 460800
      Changed.
      Configuring flash size...
      Flash will be erased from 0x00000000 to 0x0015afff...
      Compressed 1419328 bytes to 855976...
      Wrote 1419328 bytes (855976 compressed) at 0x00000000 in 23.8 seconds (effective 476.3 kbit/s)...
      Hash of data verified.
      
      Leaving...
      Hard resetting via RTS pin...
      ```
  
* 编程交互：
  
  * Windows、macOS环境：
  
      * 使用`Mu-Editor`与设备交互；**暂时不要使用VsCode + Haas Studio，因为用了也没用**！
  
      * 打开菜单左上角 `模式` 选择 `Esp MicroPython`，然后按照下面的方式即可运行代码：
  
        <img src="编码工具/Mu.Editor.png" alt="Mu.Editor" style="zoom: 25%;" />
  
      * 编写代码，点菜单 `运行`，主板蓝灯亮起，点灯成功：
  
      ```
      from machine import Pin
      # 3 红 4 绿 5 蓝
      p4=Pin(5, Pin.OUT)
      p4.on()
      ```
  
  * Linux环境：
  
    * 操作参考上述Windows环境
  
* 学习：

  * 阅读 学习材料/《MicroPython从0到1》v5.2--01Studio团队编著.pdf，看第一章和第四章
  * 学习 https://g.alicdn.com/HaaSAI/PythonDoc/library/index.html 中的1、2、3；外部设备控制，如控制LED、读取按键等，主要看3-machine硬件相关函数


### 控制LED：

* ESP32-C3开发板可以控制的LED:

  * 三色LED：GPIO3、GPIO4、GPIO5
  * 暖色：GPIO18
  * 冷色：GPIO19
  * 说明：开发板背面，标注了对应的端口

* 操作指令：

  ```
  # 引用模块
  from machine import Pin
  
  # 初始化
  p3=Pin(3, Pin.OUT)    # 表示把GPIO3设置为输出状态
  p4=Pin(4, Pin.OUT)
  p5=Pin(5, Pin.OUT)
  p18=Pin(18, Pin.OUT)
  p19=Pin(19, Pin.OUT)
  
  # 点亮
  p3.on()
  p4.on()
  p5.on()
  p18.on()
  p19.on()
  
  # 熄灭 GPIO3、GPIO4、GPIO5可以交替on、off，观察LED的变化
  p3.off()
  p4.off()
  p5.off()
  p18.off()
  p19.off()
  ```




### LED闪烁：注意缩进必须全部使用空格，不得使用tab

```
# 引用模块
from machine import Pin
import time

# 初始化
p3=Pin(3, Pin.OUT)    # 表示把GPIO3设置为输出状态

# 循环处理
while True:
    p3.on()
    time.sleep(1)    # 延时1秒
    p3.off()
    time.sleep(1)
```





### LED 交替：注意缩进必须全部使用空格，不得使用tab

```
# 引用模块
from machine import Pin
import time

# 初始化
p3=Pin(3, Pin.OUT)    # 表示把GPIO3设置为输出状态
p4=Pin(4, Pin.OUT)
p5=Pin(5, Pin.OUT)

# 循环处理
times=0
while True:
    p3.off()
    p4.off()
    p5.off()
    if times % 3 == 0:
        p3.on()
    elif times % 3 == 1:
        p4.on()
    elif times % 3 == 2:
        p5.on()
    times+=1
    
    time.sleep(1)
```



### 例子程序：

* Hello World

* LED控制

* LED循环控制

* WiFi连接

* mqtt通讯

* mqtt控制LED

* mqtt控制多个LED

* 手机App组态控制LED

* 温湿度读取

* mqtt接收温湿度信息

* 手机App组态接收温湿度信息

  <img src="App/手机端控制界面.jpeg" alt="手机端控制界面" style="zoom:25%;" />

### 补充说明：

* 本仓库中，各软件所属版权归属各软件所有者，集中于此仅用于方便大家使用，如有不适尽情告知，将立刻处理，谢谢！
