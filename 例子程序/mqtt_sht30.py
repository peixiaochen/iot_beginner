# This is your script.
from machine import Pin
import time
from umqtt.simple import MQTTClient
import sys
from sht30 import SHT30

mqtt_host = "192.168.1.15"
mqtt_client = "esp32_client"
mqtt_pub_temp = b"sensor/temp"
mqtt_pub_humi = b"sensor/humi"

# 初始化I2C：蓝板SDA->IO7，蓝板SCL->IO8
sda_pin = 7
scl_pin = 8

sensor = SHT30(scl_pin, sda_pin, 0)
addres = sensor.i2c.scan()
if len(addres) == 0:
    print("[error] no sensor")
    sys.exit(0)
else:
    if not sensor.is_present():
        sensor.i2c_addr = addres[0]

temperature,humidity  = sensor.measure()
print("temperature: %0.1f" % temperature)
print("humidity: %0.1f" % humidity)

def sub_cb(topic, msg):  # 回调函数，收到服务器消息后会调用这个函数
    print(topic, msg)

c = MQTTClient(mqtt_client, mqtt_host)  # 建立一个MQTT客户端
c.set_callback(sub_cb)  # 设置回调函数
c.connect()  # 建立连接
# c.subscribe(mqtt_sub)  # 监控ledctl这个通道，接收控制命令
while True:
    temperature,humidity  = sensor.measure()
    print("temperature: %0.1f" % temperature)
    print("humidity: %0.1f" % humidity)    

    temperature = "%0.1f" % temperature
    humidity = "%0.1f" % humidity
    c.publish(mqtt_pub_temp, temperature)    # 返回当前状态
    c.publish(mqtt_pub_humi, humidity)    # 返回当前状态
    time.sleep(5)
