import machine

interruptCounter = 0
totalInterruptsCounter = 0

# 定义中断回调
def callback(pin):
  global interruptCounter
  interruptCounter = interruptCounter+1

# 使用GPIO9-板载按钮(右键)触发中断
p9 = machine.Pin(9, machine.Pin.IN, machine.Pin.PULL_UP)
p9.irq(trigger=machine.Pin.IRQ_FALLING, handler=callback)

# 通过中断中的interruptCounter变量控制主逻辑的运行
while True:
  if interruptCounter>0:
    # 重置中断计数
    state = machine.disable_irq()
    interruptCounter = interruptCounter-1
    machine.enable_irq(state)

    # 中断总次数
    totalInterruptsCounter = totalInterruptsCounter+1
    print("Interrupt has occurred: %d" % totalInterruptsCounter)
