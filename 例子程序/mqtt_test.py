# This is your script.

import time
from umqtt.simple import MQTTClient

mqtt_host = "192.168.1.15"
mqtt_client = "esp32_client"
mqtt_sub = b"button/set"
mqtt_pub = b"button/get"


def sub_cb(topic, msg):  # 回调函数，收到服务器消息后会调用这个函数
    print(topic, msg)
    c.publish(mqtt_pub, msg)    # 返回当前状态

c = MQTTClient(mqtt_client, mqtt_host)  # 建立一个MQTT客户端
c.set_callback(sub_cb)  # 设置回调函数
c.connect()  # 建立连接
c.subscribe(mqtt_sub)  # 监控ledctl这个通道，接收控制命令
while True:
    c.check_msg()
    time.sleep(1)
