# This is your script.

from machine import Pin
import time

p4 = Pin(3, Pin.OUT)
p5 = Pin(4, Pin.OUT)
p6 = Pin(5, Pin.OUT)

mod_num = 8
cnt = 0
while True:
    print("cnt=%d mod=%d" % (cnt, cnt % mod_num))
    if cnt % mod_num == 0:
        p4.on()
        p5.off()
        p6.off()
    elif cnt % mod_num == 1:
        p4.off()
        p5.on()
        p6.off()
    elif cnt % mod_num == 2:
        p4.off()
        p5.off()
        p6.on()
    elif cnt % mod_num == 3:
        p4.on()
        p5.on()
        p6.off()
    elif cnt % mod_num == 4:
        p4.on()
        p5.off()
        p6.on()
    elif cnt % mod_num == 5:
        p4.off()
        p5.on()
        p6.on()
    elif cnt % mod_num == 6:
        p4.on()
        p5.on()
        p6.on()
    else:
        p4.off()
        p5.off()
        p6.off()

    cnt = cnt+1
    time.sleep(1)
