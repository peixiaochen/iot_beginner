# This is your script.
from machine import Pin
import time
from umqtt.simple import MQTTClient

mqtt_host = "192.168.1.15"
mqtt_client = "esp32_client"
mqtt_sub = b"button/set"
mqtt_pub = b"button/get"

p4 = Pin(3, Pin.OUT)
p5 = Pin(4, Pin.OUT)
p6 = Pin(5, Pin.OUT)

def sub_cb(topic, msg):  # 回调函数，收到服务器消息后会调用这个函数
    print(topic, msg)
    c.publish(mqtt_pub, msg)    # 返回当前状态
    if msg == b'1':
        # 开灯
        p4.on()
        p5.off()
        p6.off()
    else:
        #关灯
        p4.off()
        p5.off()
        p6.off()

c = MQTTClient(mqtt_client, mqtt_host)  # 建立一个MQTT客户端
c.set_callback(sub_cb)  # 设置回调函数
c.connect()  # 建立连接
c.subscribe(mqtt_sub)  # 监控ledctl这个通道，接收控制命令
while True:
    c.check_msg()
    time.sleep(1)
