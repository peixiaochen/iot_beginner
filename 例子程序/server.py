# request
import socket
import network
from machine import Pin
import time


red = Pin(3, Pin.OUT) # red
green = Pin(4, Pin.OUT) # green
blue = Pin(5, Pin.OUT) # blue

port = 6666  #端口号
listenSocket = None  #套接字

def pinControl(color):
    if color == b"red":
        red.on()
        green.off()
        blue.off()
        return
    elif color == b"green":
        red.off()
        green.on()
        blue.off()
        return
    elif color == b"blue":
        red.off()
        green.off()
        blue.on()
        return

try:
    # 注意：需要使用main中的wifi-connect
    wlan = network.WLAN(network.STA_IF)
    ip = wlan.ifconfig()[0]   # 本机ip
    print(ip)
    listenSocket = socket.socket()
    listenSocket.bind((ip, port))
    listenSocket.listen(1)
    listenSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print ('tcp waiting...')

    while True:
        print("accepting.....")
        conn, addr = listenSocket.accept()
        print(addr, "connected")

        while True:
            data = conn.recv(1024)
            if(len(data) == 0):
                print("close socket")
                conn.close()
                break
            pinControl(data)
            print(data)
            print("\n")
            ret = conn.send(data)
except:
    if(listenSocket):
        listenSocket.close()
