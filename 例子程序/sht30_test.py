# This is your main script.
import sys
from sht30 import SHT30

# 初始化I2C：蓝板SDA->IO7，蓝板SCL->IO8
sda_pin = 7
scl_pin = 8

sensor = SHT30(scl_pin, sda_pin, 0)
addres = sensor.i2c.scan()
if len(addres) == 0:
    print("[error] no sensor")
    sys.exit(0)
else:
    if not sensor.is_present():
        sensor.i2c_addr = addres[0]

temperature,humidity  = sensor.measure()
print("temperature: %0.1f" % temperature)
print("humidity: %0.1f" % humidity)
