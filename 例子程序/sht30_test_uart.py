from machine import UART
import time

# 初始化串口：绿板TX->IO7，绿板RX->IO8
uart1 = UART(1, baudrate=9600, rx=7, tx=8, timeout=10)

# 打个招呼：返回OK
uart1.write('AT'  +  '\r\n')
while uart1.any()==0:
    time.sleep(0.1)
print(uart1.read())

# 初始化状态：返回INIT SUCCES
uart1.write('AT+INIT'  +  '\r\n')
while uart1.any()==0:
    time.sleep(0.1)
print(uart1.read())

# 读取数据：返回温湿度数据
uart1.write('AT+PRATE=0'  +  '\r\n')
while uart1.any()==0:
    time.sleep(0.1)

print(uart1.read())

